<?php
/*
This is a sample local-config.php file
In it, you *must* include the four main database defines

You may include other settings here that you only want enabled on your local development checkouts
*/


// =========
// Debugging
// =========

define( 'LOCAL_FULL_DEBUG', false ); // Enable all local debugging at once


// =======================
// Local Database Settings
// =======================

define( 'WP_LOCAL_DB', true ); // Use a local database? If false, you must provide remote MySQL settings below
define( 'LOCAL_DB_NAME', 'local_db_name' );
define( 'LOCAL_DB_USER', 'local_db_user' );
define( 'LOCAL_DB_PASSWORD', 'local_db_password' );
define( 'LOCAL_DB_HOST', 'localhost' );






/**
 * =============================================
 * =============================================
 * DO NOT MODIFY BELOW * * *
 * Unless you're sure you know what you're doing
 * =============================================
 * =============================================
 **/

// ========================================================
// Modify site URL and home URL to match the current server
// ========================================================
// 
define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp' );
define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] );



// ===================================
// Modify database connection settings
// ===================================

if( WP_LOCAL_DB == true ) {

	define( 'DB_NAME', LOCAL_DB_NAME );
	define( 'DB_USER', LOCAL_DB_USER );
	define( 'DB_PASSWORD', LOCAL_DB_PASSWORD );
	define( 'DB_HOST', LOCAL_DB_HOST );

} else {
	
	// Note, your server must be configured to allow remote Mysql or this won't work.
	// In any case, remote Mysql is pretty slow, so using a local db is preferred.
	
	define( 'DB_NAME', 'remote_db_name' );
	define( 'DB_USER', 'remote_db_user' );
	define( 'DB_PASSWORD', 'remote_db_password' );
	define( 'DB_HOST', 'remote_db_host' );
	
}



// ===============
// Local Debugging
// ===============


if( LOCAL_FULL_DEBUG === true ) {
	define('LOCAL_WP_DEBUG', true); // Enable debug mode
	define('LOCAL_WP_DEBUG_LOG', true); // Log debug to debug.log
	define('LOCAL_WP_DEBUG_DISPLAY', true ); // Display bugs in HTML
} else {
	define('LOCAL_WP_DEBUG', false);
	define('LOCAL_WP_DEBUG_LOG', false);
	define('LOCAL_WP_DEBUG_DISPLAY', false );
}

define('LOCAL_SAVEQUERIES', false); // Save queries in the wp global $wpdb->queries. Use this sparingly, it will impact performance

