# WordPress Skeleton, Fork

Based on Mark Jaquith's [Wordpress-Skeleton](https://github.com/markjaquith/WordPress-Skeleton):

This is simply a skeleton repo for a WordPress site. Use it to jump-start your WordPress site repos, or fork it and customize it to your own liking!

## Assumptions

* Wordpress Foundation Starter Theme as a Git submodule in `/content/themes/`
* WordPress as a Git submodule in `/wp/`
* Custom content directory in `/content/` (cleaner, and also because it can't be in `/wp/`)
* `wp-config.php` in the root (because it can't be in `/wp/`)

## Instructions

* To install, run `git clone --recursive https://bitbucket.org/andrewcroce/wordpress-skeleton.git` (recursive command is necessary to install with submodules)
* BEFORE running the WP install process, rename wp-config-sample.php to wp-config.php and enter all the database connection info (you already created an empty database, right?)
* On your local copy (you're developing locally, right?) rename local-config-sample.php to local-config.php to use your local domain and customize database connection info